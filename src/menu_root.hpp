////////////////////////////////////////////////////////////////////
class MenuSceneEmpty : public MenuScene {};
////////////////////////////////////////////////////////////////////
class MenuSceneInit : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_about, btn_play, btn_exit;
    gfxBitmapFont small;

    void init()
    {
        super::init();
        title = "Main Menu";
        init_button(&btn_play, "Play", EVENT_PLAY);
        init_button(&btn_about, "About", EVENT_ABOUT);
        distribute_v(2, 0.15f, -0.15f, &btn_play.pos, &btn_about.pos);
        small = font;
        small.scale *= 0.7f;
    }

    void draw()
    {
        super::draw();
        draw_start();
        draw_end();
    }
};
////////////////////////////////////////////////////////////////////
class MenuScenePause : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_menu, btn_play, btn_restart;

    void init()
    {
        super::init();
        title = "Pause";
        init_button(&btn_play, "Continue", EVENT_BACK);
        init_button(&btn_menu, "Menu", EVENT_MENU);
        init_button(&btn_restart, "Restart", EVENT_RESTART);
        distribute_v(3, 0.3f, -0.3f, &btn_play.pos, &btn_restart, &btn_menu.pos);
    }
};
////////////////////////////////////////////////////////////////////
class MenuSceneAbout : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_back;
    gfxBitmapFont small;

    void init()
    {
        super::init();
        title = "About";
        init_button(&btn_back, "Back", EVENT_BACK);
        btn_back.pos.y = back_y;
        small = font;
        small.scale *= 0.7f;
    }

    void draw()
    {
        super::draw();
        draw_start();
        draw_end();
    }
};
////////////////////////////////////////////////////////////////////
