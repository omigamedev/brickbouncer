class Main : public gfxScene, public gfxGuiDelegate
{
public:
    typedef gfxScene super;

    static Main *s;
    gfxBitmapFont m_font;
    gfxSpriteMultipart m_bg;
    gfxSprite m_title;
    float m_width, m_height;
    float m_bgThetha;
    int m_screen_width, m_screen_height;
    int m_sound_splat;
    int m_sound_click;
    int m_time;
    int m_resume;
    int m_redraw;
    int m_level;

    // gui
    gfxGuiButton m_button_exit;

    gfxSpriteMultipart sprites;
    //int snd_button;

    gfxScene *scenes[15];
    MenuSceneInit s_init;
    MenuSceneGame s_game;
    MenuScenePause s_pause;
    MenuSceneAbout s_about;

    Main()
    {
        m_sound_splat = 0;
        m_time = 0;
        m_resume = 0;
        m_level = 0;
        m_bgThetha = 0.0f;
        m_font = gfxBitmapFont();
    }

    void onInit(float video_width, float video_height, int screen_width, int screen_height)
    {
        m_width = video_width;
        m_height = video_height;
        m_screen_width = screen_width;
        m_screen_height = screen_height;

        //sprites.load("sprites.png");
        //m_title.load("title.png");
        //m_title.scale = 1.0f;
        //m_title.pos = gfxVector2D(0.0f, m_height - 0.2f);
        m_bg.load("stripes.png");
        m_bg.scale = 1.0f / m_height;

        m_font.load("whitefont.png", 20, 28);
        m_font.scale = 0.025f;

        gfxGui::defaultFont = m_font;
        gfxGuiButton::defaultTheme.load("button.png");
        //gfxGuiButton::defaultTheme.set_region(2, 2, 60, 60);
        gfxGuiButton::defaultTheme.scale = 0.4f;

        MenuScene::m_height = m_height;
        MenuScene::m_width = m_width;
        MenuScene::title_y = m_height - 0.1f;
        MenuScene::client_bottom = -m_height;
        MenuScene::client_top = MenuScene::title_y - 0.2f;
        MenuScene::back_y = -m_height + 0.1f;
        MenuScene::font = m_font;

        // enable audio
        //m_sound_splat = engine_audio_sound_load("splat.raw");
        m_sound_click = engine_audio_sound_load("click.raw");

        // init sub-scenes once
        if(!m_resume)
        {
            gfxGui::init_handler();

            subscenes = scenes;
            memset(scenes, 0, sizeof(scenes));
            add(&s_init);
            add(&s_game);
            add(&s_pause);
            add(&s_about);
            s_init.init();
            history[0] = index_of(&s_init);
        }
        else
        {
            resume();
        }

        MenuScene *current = (MenuScene*)scenes[history[history_index]];
        current->enable();

        m_time = 0;
        m_redraw = 1;
        m_resume = 1; // from here every call of onInit is a resume
        LOG("initted");
    }
    void resume()
    {
        super::resume();
        LOG("--------> APP resume");
        
        m_bg.load("stripes.png");
        m_bg.scale = 1.0f;// / m_height;
        
        sprites.load("sprites.png");
        
        m_font.load("whitefont.png", 20, 28);
        m_font.scale = 0.025f;
        
        gfxGui::defaultFont = m_font;
        gfxGuiButton::defaultTheme.load("button.png");
        //gfxGuiButton::defaultTheme.set_region(2, 2, 60, 60);
        gfxGuiButton::defaultTheme.scale = 0.4f;
        
        MenuScene::font = m_font;
        
        MenuScene *current = (MenuScene*)scenes[history[history_index]];
        current->enable();
   }
    void on_scene_delegate(class gfxScene *sender, GFX_SCENE_EVENT event)
    {
        //if(!delegate) return;
        engine_audio_sound_play(m_sound_click, 1.0f);
        switch(event)
        {
            case MenuScene::EVENT_PLAY:
                navigate(&s_game, FX_FADE_BLACK);
                break;
            case MenuScene::EVENT_PAUSE:
                engine_audio_sound_play(m_sound_click, 1.0f);
                navigate(&s_pause, FX_SWAP, 1, 10, 1);
                break;
            case MenuScene::EVENT_MENU:
                back(&s_init);
                break;
            case MenuScene::EVENT_RESTART:
                s_game.reset();
                back();
                break;
            case MenuScene::EVENT_WIN:
                s_game.reset();
                break;
            case MenuScene::EVENT_ABOUT:
                navigate(&s_about, FX_SLIDE_O);
                break;
            case MenuScene::EVENT_EXIT:
                //navigate(&s_ship, FX_SLIDE_O);
                break;
            case MenuScene::EVENT_BACK:
                back();
                break;
        }
    }
    void onDraw(int ms)
    {
        glLoadIdentity();
        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        
        gfxScene *current = scenes[history[history_index]];
        if(current == &s_game)
        {
            m_time += ms;
            if(m_time > 100)
            {
                m_time = m_time % 100;
                s_game.timerTick();
            }
        }

        glClear(GL_COLOR_BUFFER_BIT);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        
        m_bg.tshift.v = sinf(m_bgThetha) * m_height * 0.3f;
        m_bgThetha += 0.005f;
        m_bg.draw();
        //m_title.draw();
        gfxScene::draw();
        m_redraw = 0;
    }
    void onPause()
    {
        LOG("pause");
        gfxScene *current = scenes[history[history_index]];
        if(current == &s_game)
        {
            navigate(&s_pause, FX_SWAP, 1, 10, 1);
        }
        m_font.tex.unload();
        destroy();
    }
    void onDestroy()
    {
        LOG("destroy");
    }
    void onTouchDown(int x, int y, int tid)
    {
        gfxVector2D p = screen_to_gl(x, y);
        if(gfxGui::handle_touch(T_GUI_TOUCH_DOWN, p.x, p.y, tid) == 0)
        {
            //engine_audio_sound_play(m_sound_splat, 0.5f);
        }
        m_redraw = 1;
    }
    void onTouchMove(int x, int y, int tid)
    {
        gfxVector2D p = screen_to_gl(x, y);
        if(gfxGui::handle_touch(T_GUI_TOUCH_MOVE, p.x, p.y, tid) == 0)
        {
        }
        m_redraw = 1;
    }
    void onTouchUp(int x, int y, int tid)
    {
        gfxVector2D p = screen_to_gl(x, y);
        if(gfxGui::handle_touch(T_GUI_TOUCH_UP, p.x, p.y, tid) == 0)
        {

        }
        m_redraw = 1;
    }
    int onKeyDown(int code)
    {
        // ignore while animating
        if(anim_counter > 0  && anim_counter < anim_duration) return 1;
        if(code == 4)
        {
            gfxScene *current = scenes[history[history_index]];
            if(current == &s_game)
            {
                engine_audio_sound_play(m_sound_click, 1.0f);
                navigate(&s_pause, FX_SWAP, 1, 10, 1);
                return 1;
            }
            if(current == &s_pause || current == &s_about)
            {
                engine_audio_sound_play(m_sound_click, 1.0f);
                back(&s_init);
                return 1;
            }
            LOG("EXIT");
        }
        return 0;
    }
    int onKeyUp(int code)
    {
        return 0;
    }
    void onAccelerate(float x, float y, float z)
    {
        s_game.acc[0] = x;
        s_game.acc[1] = y;
        s_game.acc[2] = z;
    }

    gfxVector2D screen_to_gl(int x, int y)
    {
        float fx = (float)x / (float)m_screen_width * 2.0f * m_width - m_width;
        float fy = -((float)y / (float)m_screen_height * 2.0f * m_height - m_height);
        return gfxVector2D(fx, fy);
    }

    gfxScreenCoord gl_to_screen(float x, float y)
    {
        int fx = (x + m_width) * 0.5f * m_screen_width;
        int fy = (-y / m_height + 1.0f) * 0.5f * m_screen_height;
        return gfxScreenCoord(fx, fy);
    }
};
