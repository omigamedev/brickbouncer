#include <engine/engine.h>
#include <engine/engine_gui.h>
#include <engine/engine_scene.h>
#include "menu_base.hpp"
#include "menu_root.hpp"
#include "game.hpp"
#include "app.hpp"
#include <stdarg.h>
#include <math.h>

float MenuScene::m_height;
float MenuScene::m_width;
float MenuScene::client_top;
float MenuScene::client_bottom;
float MenuScene::title_y;
float MenuScene::back_y;
gfxBitmapFont MenuScene::font;

Main *Main::s = NULL;
engine_video_t *g_video;

void app_main()
{
    LOG("main");
    if(Main::s == NULL)
    {
        Main::s = new Main();
    }
    else
    {
        LOG("resume");
    }
    LOG("sizeof(gfxVector2D)=%lu", sizeof(gfxVector2D));
}
void app_init(engine_video_t *video)
{
    g_video = video;
    float ratio = (float)video->screen_width / (float)video->screen_height;
    float hh = 1.0f;
    float ww = hh * ratio;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(-ww, ww, -hh, hh, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);

    for(int i = 0; i < 0xFFFF; i++) glDisable(i);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    Main::s->onInit(hh, ww, video->screen_height, video->screen_width);
}
void app_draw(int ms)
{
    Main::s->onDraw(ms);
}
void app_pause()
{
    Main::s->onPause();
}
void app_resume()
{
    Main::s->resume();
}
void app_destroy()
{
    Main::s->onDestroy();
    delete Main::s;
    Main::s = NULL;
}
void app_touch_down(int x, int y, int tid)
{
    Main::s->onTouchDown(y, g_video->screen_width - x, tid);
}
void app_touch_move(int x, int y, int tid)
{
    Main::s->onTouchMove(y, g_video->screen_width - x, tid);
}
void app_touch_up(int x, int y, int tid)
{
    Main::s->onTouchUp(y, g_video->screen_width - x, tid);
}
void app_accelerate(float x, float y, float z)
{
    Main::s->onAccelerate(x, y, z);
}
int app_key_down(int code)
{
    return Main::s->onKeyDown(code);
}
int app_key_up(int code)
{
    return Main::s->onKeyUp(code);
}
