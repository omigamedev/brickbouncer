#define NBALLS 1000
#define NCOINS 1000
#include <math.h>
#include <Box2D/Box2D.h>

static float rad2deg = 180.0f / b2_pi;
static float deg2rad = b2_pi / 180.0f;
static float gl2box = 10.0f / 1.0f;
static float box2gl = 1.0f / 10.0f;

class Brick
{
public:
    b2Body *m_body;
    b2Vec2 m_pos;
    b2Vec2 m_size;
    float m_rot;
    int m_active;
    int m_hole;
    
    Brick() : m_active(0), m_rot(0.0f), m_hole(0)
    {
    }
    
    void init(b2World *world)
    {
        b2BodyDef bdef;
        bdef.position = m_pos;
        bdef.angle = m_rot;
        bdef.userData = this;
        m_body = world->CreateBody(&bdef);
        b2PolygonShape bshape;
        bshape.SetAsBox(m_size.x, m_size.y);
        m_body->CreateFixture(&bshape, 1.0f);
        m_active = 1;
    }
    
    void remove(b2World *world)
    {
        world->DestroyBody(m_body);
        m_active = 0;
    }
    
    void draw()
    {
        b2Vec2 pos = box2gl * m_body->GetPosition();
        float angle = (int)(m_body->GetAngle() * rad2deg) % 360;
        b2Fixture *flist = m_body->GetFixtureList();
        b2PolygonShape *shape = (b2PolygonShape*)flist[0].GetShape();
        
        glBindTexture(GL_TEXTURE_2D, 0);
        
        glPushMatrix();
        //glEnableClientState(GL_VERTEX_ARRAY);
        
        glTranslatef(pos.x, pos.y, 0.0f);
        glRotatef(angle, 0.0f, 0.0f, 1.0f);
        glScalef(box2gl, box2gl, box2gl);
        glVertexPointer(2, GL_FLOAT, 0, shape->m_vertices);
        
        glPointSize(4.0f);
        glLineWidth(3.0f);
        if(!m_hole) glDrawArrays(GL_TRIANGLE_STRIP, 0, shape->m_vertexCount);
        glDrawArrays(GL_POINTS, 0, shape->m_vertexCount);
        
        //glDisableClientState(GL_VERTEX_ARRAY);
        glPopMatrix();        
    }
};

class Ball
{
public:
    b2Body *m_body;
    b2Vec2 m_vertex[16];
    
    void init(b2World *world, const b2Vec2 &pos, float radius)
    {
        b2BodyDef bdef;
        bdef.position = pos;
        bdef.type = b2_dynamicBody;
        m_body = world->CreateBody(&bdef);
        b2CircleShape bshape;
        bshape.m_radius = radius;
        b2FixtureDef fdef;
        fdef.density = 1.0f;
        fdef.restitution = 0.0f;
        fdef.friction = 1.0f;
        fdef.shape = &bshape;
        m_body->CreateFixture(&fdef);
        
        float theta_step = b2_pi / 8.0f;;
        for(int i = 0; i < 16; i++)
        {
            m_vertex[i] = radius * b2Vec2(cosf(theta_step * i), sinf(theta_step * i));
        }
    }
    
    void draw()
    {
        b2Vec2 pos = box2gl * m_body->GetPosition();
        int angle = (int)(m_body->GetAngle() * rad2deg) % 360;
        //LOG("angle %d", angle);
        
        glPushMatrix();
        //glEnableClientState(GL_VERTEX_ARRAY);
        
        glTranslatef(pos.x, pos.y, 0.0f);
        glRotatef((float)angle, 0.0f, 0.0f, 1.0f);
        glScalef(box2gl, box2gl, box2gl);
        glVertexPointer(2, GL_FLOAT, 0, m_vertex);
        
        glPointSize(3.0f);
        glLineWidth(1.0f);
        glDrawArrays(GL_LINE_LOOP, 0, 16);
        glDrawArrays(GL_POINTS, 0, 16);
        
        //glDisableClientState(GL_VERTEX_ARRAY);
        glPopMatrix();        
    }
};

class gfxSpriteAnimation : public gfxSpriteMultipart
{
public:
	int frame;
	int delay;
	int delay_counter;
	int cols, rows;
	
    gfxSpriteAnimation()
    {
        frame = -1;
        delay = 0;
        delay_counter = 0;
    }
    void draw()
    {
        if(frame < rows * cols && frame > -1)
        {
            int nx = frame % cols * tex.width/cols;
            int ny = (int)floor((float)frame / (float)cols) * tex.height/rows;
            set_region(nx, ny, tex.width/cols, tex.height/rows);
        }
        gfxSpriteMultipart::draw();
    }
    void step(float speed)
    {
        int d = (int)((float)this->delay / speed);
        if(delay_counter == d)
        {
            delay_counter = d;
            if(frame < rows * cols)
            {
                frame++;
            }
        }
        else if(delay_counter < d)
        {
            delay_counter++;
        }
        else
        {
            delay_counter = 0;
            if(frame < rows * cols)
            {
                frame++;
            }
        }
    }
    int finished()
    {
        return frame == -1 || frame == rows * cols;
    }
};

////////////////////////////////////////////////////////////////////////////////////////

class MenuSceneGame : public MenuScene, public gfxGuiObject
{
public:
    typedef MenuScene super;
    gfxGuiButton btnPause;
    gfxSprite m_spriteBrick;
    gfxSprite m_spriteBall;
    gfxSprite m_spriteClock;
    gfxSprite m_spriteTotcoins;
    gfxSpriteMultipart m_bgf, m_bgb;
    gfxSpriteAnimation m_coins[NCOINS];
    gfxSpriteAnimation m_fire;
    b2World *m_b2world;
    Brick m_bricks[NBALLS];
    Ball m_ball;
    int m_touchJump;
    int m_touchFire;
    int m_active;
    int m_jump;
    int m_lastContact;
    int m_soundBoing;
    int m_soundFloor;
    int m_soundAmbient;
    int m_soundAmbientLoop;
    int m_soundCoin;
    int m_hitFloor;
    int m_time;
    int m_totcoins;
    char m_str_time[32];
    char m_str_coins[32];
    float acc[3];
    b2Vec2 m_lastPos;
    b2Vec2 m_force;

    MenuSceneGame()
    {
        m_active = 1;
        m_jump = 0;
        m_time = 0;
        m_totcoins = 0;
        m_force = b2Vec2();
        m_lastPos = b2Vec2();
        m_hitFloor = 1;
        m_touchFire = 0;
        m_touchJump = 0;
    }

    void init()
    {
        super::init();
        
        m_b2world = new b2World(b2Vec2(0.0f, -40.0f));
        m_b2world->SetAllowSleeping(true);
        
        //b.init(world, b2Vec2(0.0f, 0.0f), b2Vec2(1.0f, 1.0f));
        m_ball.init(m_b2world, b2Vec2(-2.0f, 3.0f), 1.0f);
        
        float shift = 0.0f;
        for(int i = 0; i < NBALLS; i++)
        {
            float y = -5.0f + randf() * 3.0f;
            m_bricks[i].m_pos = b2Vec2(5.0f * i - 2.0f + shift, y);
            m_bricks[i].m_size = b2Vec2(2.0f, 0.5f);
            m_bricks[i].m_rot = randfneg() * 0.2f;
            m_bricks[i].init(m_b2world);
            shift += randf() * 5.0f;
        }
        
        btnPause.set_text("Pause");
        btnPause.labelColor = gfxColor(1.0f, 1.0f, 1.0f, 1.0f);
        btnPause.width = 0.2f;
        btnPause.height = 0.08f;
        btnPause.pos.x = 0.78f;
        btnPause.pos.y = m_height - 0.1f;
        add_gui_button(&btnPause, EVENT_PAUSE);
        
        resume();
    }

    void reset()
    {
        m_b2world->DestroyBody(m_ball.m_body);
        m_ball.init(m_b2world, b2Vec2(-2.0f, 3.0f), 1.0f);
        m_lastPos = b2Vec2();
        m_hitFloor = 1;
        m_time = 0;
        m_totcoins = 0;
    }
    
    void respawn()
    {
        m_b2world->DestroyBody(m_ball.m_body);
        m_ball.init(m_b2world, b2Vec2(m_lastPos.x - 2.0f, 3.0f), 1.0f);
        m_hitFloor = 1;
    }
    
    void resume()
    {
        m_bgf.load("background-front.png");
        m_bgf.scale = m_height / m_bgf.ratio;
        m_bgb.load("background-back.png");
        m_bgb.scale = m_height / m_bgb.ratio;
        m_spriteBrick.load("brick.png");
        m_spriteBall.load("ball.png");
        m_spriteClock.load("clock.png");
        m_spriteClock.scale = 0.06f;
        m_spriteClock.pos = gfxVector2D(-0.9f, m_height - 0.1f);
        m_spriteTotcoins.load("totcoins.png");
        m_spriteTotcoins.scale = 0.07f;
        m_spriteTotcoins.pos = gfxVector2D(-0.2f, m_height - 0.1f);
        m_soundBoing = engine_audio_sound_load("jump.raw");
        m_soundFloor = engine_audio_sound_load("touch-floor.raw");
        m_soundAmbient = engine_audio_sound_load("ambient.raw");
        m_soundAmbientLoop = engine_audio_sound_loop(m_soundAmbient, 0.6f);
        m_soundCoin = engine_audio_sound_load("coin.raw");
        
        m_fire.load("fire.png");
        m_fire.cols = 4;
        m_fire.rows = 3;
        m_fire.scale = 0.07f;
        m_fire.rot = 160.0f;
        
        gfxSpriteAnimation coin;
        coin.load("coins.png");
        coin.cols = 8;
        coin.rows = 8;
        coin.scale = 0.05f;
        
        for(int i = 0; i < NCOINS; i++)
        {
            m_coins[i] = coin;
            m_coins[i].pos = gfxVector2D(i * 0.4f, randf() * 0.3f);
            m_coins[i].frame = rand() % (8 * 8);
        }
        
        super::resume();
    }

    void destroy()
    {
        engine_audio_sound_unload(m_soundBoing);
        engine_audio_sound_unload(m_soundFloor);
        engine_audio_sound_unload(m_soundAmbient);
        engine_audio_sound_loop_stop(m_soundAmbientLoop);
        engine_audio_sound_unload(m_soundCoin);
        m_bgf.tex.unload();
        m_bgb.tex.unload();
        m_spriteBrick.tex.unload();
        m_spriteBall.tex.unload();
        m_coins[0].tex.unload();
        m_spriteClock.tex.unload();
        m_spriteTotcoins.tex.unload();
        delete m_b2world;
        
        super::destroy();
    }

    void disable()
    {
        engine_audio_sound_loop_pause(m_soundAmbientLoop);
        m_active = 0;
        unhandle_touch();
        super::disable();
    }

    void enable()
    {
        handle_touch();
        super::enable();
        m_active = 1;
        engine_audio_sound_loop_resume(m_soundAmbientLoop);
    }

    int on_touch(T_GUI type, float x, float y, int id)
    {
        int ret = GFX_GUI_UNHANDLED;

        if(type == T_GUI_TOUCH_DOWN)
        {
            m_force.x = x > 0.0f ? 10.0f : -10.0f;
            m_touchJump = 1;
            m_fire.frame = 0;
        }
        else if(type == T_GUI_TOUCH_MOVE)
        {
            m_force.x = x > 0.0f ? 10.0f : -10.0f;
        }
        else if(type == T_GUI_TOUCH_UP)
        {
            m_jump = 0;
            m_touchJump = 0;
            m_touchFire = 0;
        }
        return ret;
    }
    
    void update()
    {
        static float time_step = 1.0f / 60.0f;
        
        for(int i = 0; i < NBALLS; i++)
        {
            float diff = m_bricks[i].m_pos.x - m_ball.m_body->GetPosition().x;
            if(fabsf(diff) < 20)
            {
                //if(!m_bricks[i].m_active) m_bricks[i].init(m_b2world);
            }
            else 
            {
                //if(m_bricks[i].m_active) m_bricks[i].remove(m_b2world);
            }
            m_bricks[i].m_body->SetActive(fabsf(diff) < 20 && !m_bricks[i].m_hole);
            m_bricks[i].m_active = fabsf(diff) < 20;
        }
        
        b2ContactEdge *contacts =  m_ball.m_body->GetContactList();
        if(contacts != NULL && contacts->contact->IsTouching())
        {
            m_lastContact = 0;
            m_touchFire = 0;
            Brick *brick = (Brick*)contacts->other->GetUserData();
            m_lastPos.x = brick->m_body->GetPosition().x;
            if(m_hitFloor == 1)
            {
                engine_audio_sound_play(m_soundFloor, 0.8f);
                m_hitFloor = 0;
            }
        }
        else 
        {
            m_hitFloor = 1;
            m_lastContact++;
            if(m_lastContact >= 15)
            { 
                m_touchFire = m_touchJump;
            }
        }
        if(contacts != NULL && contacts->contact->IsTouching() && m_jump == 0) 
            m_jump = 5;
        
        if(m_touchJump && m_jump > 0) 
        {
            if(m_jump == 5)
            {
                m_ball.m_body->ApplyLinearImpulse(b2Vec2(0.0f, 50.0f), m_ball.m_body->GetWorldCenter());
                engine_audio_sound_play(m_soundBoing, 1.0f);
            }
            //else m_ball.m_body->ApplyForceToCenter(b2Vec2(0.0f, 5.0f));
            m_jump--;
        }
        m_ball.m_body->ApplyTorque(-50.0f);
        
        float angle = 180.0f - acc[1] * 5.0f;
        
        if(m_touchFire)
        {
            m_fire.visible = 1;
            m_fire.step(1.0f);
            if(m_fire.frame >= 7) m_fire.frame = 4;
            m_ball.m_body->ApplyForceToCenter(b2Vec2(sinf(angle * deg2rad) * 100.0f, 100.0f));
        }
        else if(!m_fire.finished())
        {
            m_fire.step(1.0f);
        }
        else
        {
            m_fire.visible = 0;
        }
        
        m_b2world->Step(time_step, 6, 2);
        
        b2Vec2 pos = m_ball.m_body->GetPosition();
        m_fire.pos.x = pos.x * box2gl - sinf(angle * deg2rad) * m_fire.width() * 2.0f;
        m_fire.pos.y = pos.y * box2gl + cosf(angle * deg2rad) * m_fire.height() * 2.0f;
        m_fire.rot = angle;
        
        // limit ball velocity
        if(m_ball.m_body->GetAngularVelocity() > 8.0f)
        {
            m_ball.m_body->SetAngularVelocity(8.0f);
        }
        else if(m_ball.m_body->GetAngularVelocity() < -8.0f)
        {
            m_ball.m_body->SetAngularVelocity(-8.0f);
        }
        
        // dead test
        if(m_ball.m_body->GetPosition().y < -10.0f) respawn();
        
        m_bgf.tshift.u = m_ball.m_body->GetPosition().x * box2gl * 0.04f;
        m_bgb.tshift.u = m_ball.m_body->GetPosition().x * box2gl * 0.01f;
        
        // update timer text
        int mm, ss, ms;
        int sec = m_time / 10;
        ms = m_time % 10;
        mm = sec / 60;
        ss = sec - mm * 60;
        sprintf(m_str_time, "%02d:%02d:%d", mm, ss, ms);
        
        // update timer text
        sprintf(m_str_coins, "%d", m_totcoins);
        
        // coins animation
        for(int i = 0; i < NCOINS; i++)
        {
            if(m_coins[i].frame != -2)
            {
                m_coins[i].step(1.0f);
                if(m_coins[i].finished()) m_coins[i].frame = 0;
            }
        }
    }

    void draw()
    {        
        if(m_active)
        {
            engine_audio_sound_loop_update(m_soundAmbientLoop);
            update();
        }
        
        draw_start();
        
        m_bgb.draw();
        m_bgf.draw();
        
        glPushMatrix();
        
        b2Vec2 pos = m_ball.m_body->GetPosition();
        glTranslatef(-pos.x * box2gl, 0.0f, 0.0f);
        
        for(int i = 0; i < NBALLS; i++)
        {
            if(m_bricks[i].m_active && !m_bricks[i].m_hole) 
            {
                //m_bricks[i].draw();
                b2Vec2 pos = box2gl * m_bricks[i].m_body->GetPosition();
                m_spriteBrick.pos.x = pos.x;
                m_spriteBrick.pos.y = pos.y;
                m_spriteBrick.rot = (int)(m_bricks[i].m_body->GetAngle() * rad2deg) % 360;
                m_spriteBrick.scale = m_bricks[i].m_size.x * box2gl * 1.12f;
                m_spriteBrick.draw();
            }
        }
        m_fire.draw();
        // draw ball
        {
            b2Vec2 pos = box2gl * m_ball.m_body->GetPosition();
            m_spriteBall.pos.x = pos.x;
            m_spriteBall.pos.y = pos.y;
            m_spriteBall.rot = (int)(m_ball.m_body->GetAngle() * rad2deg) % 360;
            m_spriteBall.scale = 1.0f * box2gl;
            m_spriteBall.draw();
        }
        
        for(int i = 0; i < NCOINS; i++)
        {
            if(m_coins[i].frame > -1)
            {
                float dx = m_coins[i].pos.x - pos.x * box2gl;
                float dy = m_coins[i].pos.y - pos.y * box2gl;
                float d = sqrtf(dx * dx + dy * dy);
                if(d < m_coins[i].scale + m_spriteBall.scale)
                {
                    m_coins[i].frame = -2;
                    engine_audio_sound_play(m_soundCoin, 1.0f);
                    m_totcoins++;
                }
                else if(dx < 1.0f)
                {
                    m_coins[i].draw();
                }
            }
        }
        
        glPopMatrix();
        
        draw_gui();
        
        draw_end();
    }

    void draw_gui()
    {
        gfxBitmapFont big = font;
        big.scale *= 1.3f;
        btnPause.draw();
        m_spriteClock.draw();
        m_spriteTotcoins.draw();
        big.draw_text(m_str_time, -0.8f, m_height - 0.1f);
        big.draw_text(m_str_coins, -0.05f, m_height - 0.1f);
    }

    void timerTick()
    {
        m_time++;
    }
};
