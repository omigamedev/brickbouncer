#include <stdarg.h>

class MenuScene : public gfxScene, public gfxGuiDelegate
{
protected:
    void draw_start()
    {
        glPushMatrix();
        glTranslatef(pos.x, pos.y, 0.0f);
    }
    void draw_end()
    {
        glPopMatrix();
    }

public:
    typedef gfxScene super;

    static const GFX_SCENE_EVENT EVENT_PLAY     = 0;
    static const GFX_SCENE_EVENT EVENT_ABOUT    = 1;
    static const GFX_SCENE_EVENT EVENT_EXIT     = 2;
    static const GFX_SCENE_EVENT EVENT_BACK     = 3;
    static const GFX_SCENE_EVENT EVENT_MENU     = 4;
    static const GFX_SCENE_EVENT EVENT_RESTART  = 5;
    static const GFX_SCENE_EVENT EVENT_WIN      = 6;
    static const GFX_SCENE_EVENT EVENT_PAUSE    = 7;

    static float m_height;
    static float m_width;
    static float client_top;
    static float client_bottom;
    static float title_y;
    static float back_y;
    static gfxBitmapFont font;

    gfxGuiObject *guis[32];
    gfxObject *objs[32];
    GFX_SCENE_EVENT events[32];
    int guis_index;
    const char *title;

    MenuScene()
    {
        guis_index = 0;
        title = 0;
        memset(guis, 0, sizeof(guis));
        memset(events, 0, sizeof(events));
        memset(objs, 0, sizeof(objs));
    }

    void add_object(gfxObject *obj)
    {
        objs[guis_index] = obj;
        events[guis_index] = -1;
        guis_index++;
    }
    void add_gui_object(gfxGuiObject *obj)
    {
        guis[guis_index] = obj;
        events[guis_index] = -1;
        guis_index++;
    }
    void add_gui_button(gfxGuiButton *btn, GFX_SCENE_EVENT event = -1)
    {
        guis[guis_index] = btn;
        events[guis_index] = event;
        guis_index++;
        btn->delegate = this;
    }
    int index_of(gfxGuiObject *gui)
    {
        for(int i = 0; i < guis_index; i++)
        {
            if(guis[i] == gui) return i;
        }
        return -1;
    }
    void distribute_v(int n, float y0, float y1, ...)
    {
        static gfxVector2D *pos;
        float step = (y1 - y0) / (float)(n - 1);

        va_list arglist;
        va_start(arglist, y1);
        for(int i = 0; i < n; i++)
        {
            pos = va_arg(arglist, gfxVector2D*);
            pos->y = y0 + (float)i * step;
        }
        va_end(arglist);
    }
    void distribute_v(float y0, float y1)
    {
        int i;
        for(i = 0; i < sizeof(guis) / sizeof(gfxGuiObject*) && guis[i]; i++);
        float step = i > 1 ? (y1 - y0) / (float)(i - 1) : 0.0f;
        for(int j = 0; j < i; j++)
        {
            guis[j]->pos.y = y0 + (float)j * step;
        }
    }
    void init_button(gfxGuiButton *btn, const char *label, GFX_SCENE_EVENT event)
    {
        btn->set_text(label);
        btn->labelColor = gfxColor(1.0f, 1.0f, 1.0f, 1.0f);
        btn->width = 0.4f;
        btn->height = 0.1f;
        add_gui_button(btn, event);
    }

    virtual void resume()
    {
    }
    virtual void init()
    {
    }
    virtual void draw()
    {
        super::draw();
        draw_start();
        if(title)
        {
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            font.draw_text_center(title, 0, title_y);
        }
        for(int i = 0; i < guis_index; i++)
        {
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            if(guis[i]) guis[i]->draw();
            else if(objs[i]) objs[i]->draw();
        }
        draw_end();
    }
    virtual void destroy()
    {
        super::destroy();
        disable();
    }
    virtual void enable()
    {
        LOG("enable");
        super::enable();
        for(int i = 0; i < guis_index; i++)
        {
            if(guis[i]) guis[i]->handle_touch();
        }
    }
    virtual void disable()
    {
        LOG("disable");
        super::disable();
        for(int i = 0; i < guis_index; i++)
        {
            if(guis[i]) guis[i]->unhandle_touch();
        }
    }
    virtual void on_gui_delegate(class gfxGuiObject *sender, T_GUI event, void *data)
    {
        if(!delegate || event != T_GUI_TOUCH_CLICK) return; // no delegate was set

        // find the event to be launched (if any)
        int i = index_of(sender);
        if(i == -1 || events[i] == -1) return; // no event or object not found :|

        // launch the event
        delegate->on_scene_delegate(this, events[i]);
    }
};
