START_PATH := $(call my-dir)/../../src

MYLIBS := $(call my-dir)/../../../../src
include $(MYLIBS)/Box2D/Android.mk
include $(MYLIBS)/png/Android.mk
include $(MYLIBS)/jpeg/Android.mk
include $(MYLIBS)/tremolo/Android.mk
include $(MYLIBS)/glm/Android.mk
include $(MYLIBS)/engine/Android.mk

LOCAL_PATH := $(START_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE    := brickbouncer
LOCAL_SRC_FILES := main.cpp
LOCAL_LDLIBS    := -llog -landroid -lEGL -lGLESv1_CM -lz -lOpenSLES
LOCAL_CFLAGS    := -DENGINE_ANDROID -I$(LOCAL_PATH)
LOCAL_STATIC_LIBRARIES := box2d engine
include $(BUILD_SHARED_LIBRARY)
