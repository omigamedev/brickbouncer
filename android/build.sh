#!/bin/sh
set -e
ndk-build 
ant debug -q 
adb install -r ./bin/BrickBouncer-debug.apk
adb shell am start -a android.intent.action.MAIN -n it.omam.android.BrickBouncer/android.app.NativeActivity
